const { clipboard } = require("electron");
const _ = require('underscore');
const os = require('os');

const {
    exec, spawn
} = require('child_process');
const {
    purgeOrg,
    setOrgsList,
    getCurrentWorkingOrg,
    getCurrentWorkingDir,
    getOrgInfo,
    setOrgInfo,
    setOrgDeployData,
    getLastOrgDeploy
} = require("./sessionManager");

let orgList = 'Loading';

function updateWorkingLog(update) {
    console.log(update);
}

function updateSvn() {
    let wd = getCurrentWorkingDir();
    return new Promise(function(resolve, reject) {
        exec('svn up', {cwd: wd}, (err, stdout, stderr) => {
            if (err) {
                toggleSpinner();
                console.error(err);
                reject(err);
                return;
            }
            updateWorkingLog('Updated from svn ' + new Date().toUTCString());
            resolve({});
        });
    });
}

function updateNpm() {
    let wd = getCurrentWorkingDir();
    return new Promise(function(resolve, reject) {
        exec('npm i', {cwd: wd}, (err, stdout, stderr) => {
            if (err) {
                toggleSpinner();
                console.error(err);
                reject(err);
                return;
            }
            updateWorkingLog('Updated NPM ' + new Date().toUTCString());
            resolve({});
        });
    });
}

function getSvnInfo() {
    let wd = getCurrentWorkingDir();
    return new Promise(function(resolve, reject) {
        exec('svn info', {cwd: wd}, (err, stdout, stderr) => {
            if (err) {
                toggleSpinner();
                console.error(err);
                reject(err);
                return;
            }
            resolve(stdout);
        });
    });
}

function pushToOrg(orgAlias) {
    let wd = getCurrentWorkingDir();
    console.log('wd: ' + wd)
    return new Promise(function(resolve, reject) {
        updateWorkingLog('pushing org at ' + new Date().toUTCString());
        let push = spawn('ant', ['push.all.projects', '-Dsf.org.alias='+ orgAlias], {cwd: wd});
        console.log('push: ' + push)
        push.stdout.on('data', function (data) {
            updateWorkingLog(data.toString());
        });
        push.on('close', function(code) {
            resolve(code);
        })
        push.stderr.on('data', (err) => {
            toggleSpinner();
            reject(err);
            console.error(err);
        })
    });
}

function initOrg(orgAlias) {
    let wd = getCurrentWorkingDir();
    return new Promise(function(resolve, reject) {
        updateWorkingLog('initing org at ' + new Date().toUTCString());
        let push = spawn('ant', ['scratch.org.init', '-Dsf.org.alias='+ orgAlias], {cwd: wd});
        push.stdout.on('data', function (data) {
            updateWorkingLog(data.toString());
          });
        push.on('close', function(code) {
            resolve(code);
        })
        push.stderr.on('data', (err) => {
            console.error(err);
            reject(err);
        }) 
    });           
}

module.exports.pushToOrg = function(currentOrg) {
    let wd = getCurrentWorkingDir();
    let svninfo;
    if(currentOrg && wd) {
        toggleSpinner(true);

        updateSvn()
            .then(() => updateNpm())
            .then(() => getSvnInfo())
            .then(_svninfo => {
                svninfo = _svninfo;
                return pushToOrg(currentOrg);
            })
            .then(() => {
                setOrgDeployData(currentOrg, svninfo);
                toggleSpinner();
                updateLatestDeploy();
                updateWorkingLog('Complete push to org at ' + new Date().toUTCString());
                new Notification(`Org ${currentOrg} updated`, {
                    body: `Latest code has been pushed to org ${currentOrg}`
                })       
            }
        );
    }
};

module.exports.createOrg = function () {
    toggleSpinner(true);
    let orgAlias = document.getElementById('org_alias_input').value;
  
    if(!orgAlias){
      orgAlias = 'DefaultAlias';
    }
    let wd = getCurrentWorkingDir();
    let extra = os.type() != 'Windows_NT' ? '/project/common/base' : '\\project\\common\\base'
    let createwd = wd + extra;

    updateWorkingLog('creating org at ' + new Date().toUTCString());
    exec('sfdx force:org:create -f config/project-scratch-def.json --setdefaultusername -d 30 -a ' + orgAlias, {cwd: createwd}, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
        let svninfo;
        updateSvn()
            .then(() => updateNpm())
            .then(() => getSvnInfo())
            .then(_svninfo => {
                svninfo = _svninfo;
                return pushToOrg(orgAlias);
            })
            .then(() => initOrg(orgAlias))
            .then(() => module.exports.refreshOrgLst())
            .then(() => {
                setOrgDeployData(orgAlias, svninfo);
                updateLatestDeploy();
                toggleSpinner();
            });
    });
};

module.exports.getOrgInfo = function (orgid, username) {
    let value = getOrgInfo(orgid);
    if(value && value.info) {
        clipboard.writeText(value.info);
        new Notification('Details copied to clipboard', {
            body: 'Scratch org details have been copied to the clipboard'
        })
    } else {
        toggleSpinner(true);
        exec('sfdx force:user:display -u ' + username, (err, stdout, stderr) => {
            if (err) {
                console.error(err);
                return;
            }
            setOrgInfo(orgid, stdout);
            clipboard.writeText(stdout);
            toggleSpinner();
            new Notification('Details copied to clipboard', {
                body: 'Scratch org details have been copied to the clipboard'
            })
        });
    }
}

module.exports.deleteOrg = function (username, orgid) {
    toggleSpinner(true);
    exec('sfdx force:org:delete --noprompt -u ' + username, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
        toggleSpinner();
        var elem = document.querySelector("[orgid='" + orgid + "']");
        elem.parentNode.removeChild(elem);
        purgeOrg(orgid);
    });
}

module.exports.openOrg = function (username) {
    exec('sfdx force:org:open -u ' + username, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
    });
}

module.exports.refreshOrgLst = function () {
    toggleSpinner(true);
    return new Promise(function(resolve, reject) {
        exec('sfdx force:org:list --json', (err, stdout, stderr) => {
            if (err) {
                console.error(err);
                return;
            }
            orgList = JSON.parse(stdout);
            console.log('setting orgs');
            setOrgsList(orgList.result);
            toggleSpinner();
            renderOrgTable(orgList.result.scratchOrgs);
            resolve({});
        });
    });
}

function updateLatestDeploy() {
    let deployinfo = getLastOrgDeploy();

    if(deployinfo) {
        document.querySelector(".lastdeploy .orgname").innerHTML = deployinfo.org;
        document.querySelector(".lastdeploy .date").innerHTML = deployinfo.date;
        document.querySelector(".lastdeploy code").innerHTML = deployinfo.svn;
    }
}
module.exports.updateLatestDeploy = updateLatestDeploy;

module.exports.openLastDeploy = function(orgid) {
    let deployinfo = getLastOrgDeploy(orgid);

    if(deployinfo) {
        document.querySelector(".org-last-deploy-details code").innerHTML = deployinfo.svn;
        document.querySelector(".org-last-deploy-details .date").innerHTML = deployinfo.date;
        document.querySelector(".org-last-deploy-details").classList.toggle('slds-fade-in-open');
        document.querySelector(".slds-backdrop").classList.toggle('slds-backdrop_open');
    }
}

module.exports.showHide = function(id) {    
    if (document.getElementById(id)) {
        console.log(id)
        if (document.getElementById(id+'-show').style.display != 'none') {
            document.getElementById(id+'-show').style.display = 'none';
            document.getElementById(id+'-hide').style.display = 'inline';
            document.getElementById(id).style.height = '250px';
        }
        else {
            document.getElementById(id+'-show').style.display = 'inline';
            document.getElementById(id+'-hide').style.display = 'none';
            document.getElementById(id).style.height = '0px';
        }
    }
}

function renderOrgTable(scratchOrgs) {
    let currentOrg = getCurrentWorkingOrg();
    let scratchOrgTableListString = "";

    scratchOrgs.forEach(it => {
      scratchOrgTableListString += getRow(it, true);
    });
  
    function getRow(it) {
        let orgref = it.alias && it.alias != 'undefined' ? it.alias : it.username;
        let checked = orgref == currentOrg ? 'checked="checked"': null;
        let row = `<tr orgid="${it.orgId}">
            <td>
                <button class="slds-button slds-button_icon push-to-org" onclick="pushToOrg('${orgref}')" title="Deploy code to org">
                    <svg class="slds-button__icon slds-icon_x-small" aria-hidden="true">
                        <use xlink:href="./node_modules/@salesforce-ux/design-system/assets/icons/utility-sprite/svg/symbols.svg#broadcast"></use>
                    </svg>
                </button>
            </td>
            <td scope="row" data-label="orgId">
              <div class="slds-truncate" title="Cloudhub"><a onclick="openOrg('${it.username}')" href="javascript:void(0);">${it.orgId} &rarr;</a></div>
            </td>
            <td data-label="Account Name">
              <div class="slds-truncate" title="alias">${it.alias}</div>
            </td>
            <td data-label="Close Date">
              <div class="slds-truncate" title="4/14/2015">${it.username}</div>
            </td>
            <td data-label="Last deploy">
                <button class="slds-button slds-button_icon" onclick="openLastDeploy('${it.orgId}')" title="View last deploy">
                    <svg class="slds-button__icon slds-icon_small" aria-hidden="true">
                        <use xlink:href="./node_modules/@salesforce-ux/design-system/assets/icons/standard-sprite/svg/symbols.svg#metrics"></use>
                    </svg>
                </button>
            </td>
            <td data-label="Get Info">
                <button class="slds-button slds-button_icon delete-org" onclick="getOrgInfo('${it.orgId}', '${it.username}')" title="Copy org details to clipboard">
                <span class="slds-icon_container null slds-icon__svg--default">
                    <svg class="slds-button__icon slds-icon_x-small" aria-hidden="true">
                        <use xlink:href="./node_modules/@salesforce-ux/design-system/assets/icons/utility-sprite/svg/symbols.svg#copy_to_clipboard"></use>
                    </svg>
                </span>
                </button>
            </td><td>
                <button class="slds-button slds-button_icon delete-org" onclick="deleteOrg('${orgref}', '${it.orgId}')" title="Delete org">
                    <svg class="slds-button__icon slds-icon_x-small" aria-hidden="true">
                        <use xlink:href="./node_modules/@salesforce-ux/design-system/assets/icons/utility-sprite/svg/symbols.svg#delete"></use>
                    </svg>
                </button>
              </td><tr>
            </tr>`;
      return row;
    }
    document.querySelector("#scratchOrgListTable").innerHTML = scratchOrgTableListString;
}

function toggleSpinner(show){
    if(!show){
        document.querySelector(".spinner-wrapper").style.display = 'none';
    }else{
        document.querySelector(".spinner-wrapper").style.display = 'flex';
    }
};

module.exports.renderOrgTable = renderOrgTable;