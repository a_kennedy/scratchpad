const _ = require('underscore');

let ORG_LIST;

module.exports.setOrgsList = function(orgs) {
    ORG_LIST = orgs;
    window.localStorage.setItem('scratch.orgs', JSON.stringify(orgs));
}

module.exports.getOrgsList = function() {
    if(ORG_LIST) {
        return ORG_LIST;
    } else {
        let orgs = window.localStorage.getItem('scratch.orgs');
        ORG_LIST = JSON.parse(orgs);
        return ORG_LIST;
    }
}

module.exports.getOrg = function(username) {
    if(ORG_LIST) {
        return _.find(ORG_LIST.scratchOrgs, function(org) {
            return org.username == username;
        })
    }
    return null;
}

function getOrgById(orgid) {
    if(ORG_LIST) {
        return _.find(ORG_LIST.scratchOrgs, function(org) {
            return org.orgId == orgid;
        })
    }
    return null;    
}

module.exports.getOrgById = getOrgById;

module.exports.purgeOrg = function(orgid) {
    let org = getOrgById(orgid);
    let orgref = org.alias && org.alias != 'undefined' ? org.alias : org.username;
    if(window.localStorage.getItem('current.org') == orgref) {
        window.localStorage.removeItem('current.org');
    }

    ORG_LIST.scratchOrgs = _.filter(ORG_LIST.scratchOrgs, function(org) {
        return org.orgId != orgid;
    });
    window.localStorage.setItem('scratch.orgs', JSON.stringify(ORG_LIST));
    window.localStorage.removeItem(orgid);
}

module.exports.getLastOrgDeploy = function(orgid) {
    if(orgid) {
        let orglastest = window.localStorage.getItem(orgid);
        if(orglastest) {
            return JSON.parse(orglastest).lastdeploy;
        }
    } else {
        let lastest = window.localStorage.getItem('lastdeploy');
        if(lastest) {
            return JSON.parse(lastest);
        }
    }
    return null;
}


module.exports.setOrgDeployData = function(orgref, svninfo) {
    let org = _.find(ORG_LIST.scratchOrgs, function(org) {
        let theorgref = org.alias && org.alias != 'undefined' ? org.alias : org.username;
        return theorgref == orgref;
    });
    
    let orginfo = window.localStorage.getItem(org.orgId);
    orginfo = orginfo ? JSON.parse(orginfo) : {};
    orginfo.lastdeploy = {
        date: new Date().toISOString(),
        svn: svninfo
    }
    window.localStorage.setItem(org.orgId, JSON.stringify(orginfo));

    let lastDeploy = {
        org: orgref,
        date: orginfo.lastdeploy.date,
        svn: svninfo
    }

    window.localStorage.setItem('lastdeploy', JSON.stringify(lastDeploy));
}

module.exports.setOrgInfo = function(orgid, info) {
    let orginfo = window.localStorage.getItem(orgid);
    orginfo = orginfo ? JSON.parse(orginfo) : {};
    orginfo.info = info;
    window.localStorage.setItem(orgid, JSON.stringify(orginfo));
}

module.exports.getOrgInfo = function(orgid) {
    let orginfo = window.localStorage.getItem(orgid);
    if(orginfo) {
        return JSON.parse(orginfo);
    }
    return null;
}

module.exports.getCurrentWorkingOrg = function() {
    return window.localStorage.getItem('current.org');
}

module.exports.setCurrentWorkingOrg = function(username) {
    window.localStorage.setItem('current.org', username);
}

module.exports.setCurrentWorkingDir = function(dir) {
    window.localStorage.setItem('arrakis.working.directory', dir);
}

module.exports.getCurrentWorkingDir = function() {
    return window.localStorage.getItem('arrakis.working.directory');
}

// module.exports.setSessionStore  = function(key, value) {
//     window.localStorage.setItem(key, value);
//     console.log(`set session store ${key} ~ ${value}`);
// }

// module.exports.getSessionStore = function (key) {
//     console.log(`got data for key ${key}`);
//     return window.localStorage.getItem(key);
// }

// module.exports.deleteSessionStore = function(key) {
//     console.log(`delete data for key ${key}`);
//     window.localStorage.removeItem(key);
// }

