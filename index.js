const { app, BrowserWindow, dialog } = require('electron');
const windowStateKeeper = require('electron-window-state');
const sessionStorage = require('electron-browser-storage');
const { renderOrgTable, refreshOrgLst } = require("./src/scripts/orgManager");
let mainWin;

app.getPath('userData');

function createWindow() {
    let winState = windowStateKeeper({
        defaultWidth: 1000,
        defaultHeight: 800
    });

    mainWin = new BrowserWindow({
        width: 1000, 
        height: 700,
        x: winState.x,
        y: winState.y,
        minWidth: 300,
        minHeight: 150,
        webPreferences: {
            nodeIntegration: true
        }
    });

    let wc = mainWin.webContents;

    mainWin.loadFile('index.html');

    wc.openDevTools();

    winState.manage(mainWin);

    mainWin.on('closed', () => {
        mainWin = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if(process.platform !== 'darwin') {
        app.quit();
    }
})

exports.selectDirectory = function () {
  return dialog.showOpenDialog(mainWin, {
    properties: ['openDirectory']
  })
}