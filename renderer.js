const electron = require('electron');
const remote = electron.remote;
const { dialog } = require('electron').remote;
const mainProcess = remote.require('./index');
const { getOrgsList, getCurrentWorkingDir, setCurrentWorkingDir, setCurrentWorkingOrg } = require("./src/scripts/sessionManager");
const { refreshOrgLst, getOrgInfo, deleteOrg, openOrg, renderOrgTable, pushToOrg, createOrg, updateLatestDeploy, openLastDeploy, showHide } = require("./src/scripts/orgManager");
const spinner = require("./src/scripts/spinner");

document.getOrgInfo = getOrgInfo;

document.deleteOrg = function(username, orgid) {
  dialog.showMessageBox({
    type: 'warning',
    buttons: ['Delete', 'Cancel'],
    title: 'Delete Org',
    message: `Delete and remove scratch org ${username}`,
    cancelId: 1
}).then(result => {
    if(result.response == 0) {
      deleteOrg(username, orgid);
    }
})
};

document.openOrg = openOrg;

document.refreshList = refreshOrgLst;

document.createOrg = createOrg;

document.showHide = showHide;

document.closeModal = function(modalselector) {
  document.querySelector(modalselector).classList.toggle('slds-fade-in-open');
  document.querySelector(".slds-backdrop").classList.toggle('slds-backdrop_open');  
}

document.pushToOrg = pushToOrg;

document.openLastDeploy = openLastDeploy;

//Init org table with saved data or send of request for org list
let orgs = getOrgsList();
if(orgs) {
    renderOrgTable(orgs.scratchOrgs);
} else {
    console.log('NOOO SAVED SESSION ORG DATA');
    refreshOrgLst();
}

let workingdirectory = getCurrentWorkingDir();
if(workingdirectory) {
  document.querySelector("#selected-directory").innerHTML = workingdirectory;
}

updateLatestDeploy();

document.getElementById('selected-directory').addEventListener('click', _ => {
  mainProcess.selectDirectory().then(result => {
    console.log(result);

    if(!result.canceled && result.filePaths && result.filePaths.length) {
        setCurrentWorkingDir(result.filePaths[0]);
        document.querySelector("#selected-directory").innerHTML = result.filePaths[0];
    }
  })
})
